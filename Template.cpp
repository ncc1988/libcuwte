/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "Template.h"


using namespace Cuwte;


Template::Template(
    const std::string& source,
    std::shared_ptr<Parser> parser
    )
    : source(source),
      parser(parser)
{
    //Nothing else at the moment.
}


void Template::read()
{
    if (this->first_template_part != nullptr) {
        //The template has already been read.
        //Nothing left to do.
        return;
    }

    if (this->source.empty()) {
        //TODO: throw Cuwte::TemplateException: no source data provided
        return;
    }
    if (!this->parser) {
        //TODO: throw Cuwte::TemplateException: no parser specified
        return;
    }

    this->first_template_part = this->parser->parse(this->source);
}


std::shared_ptr<Template> Template::readFromFile(
    const std::string& file_name,
    std::shared_ptr<Parser> parser
    )
{
    if (file_name.empty() || (parser == nullptr)) {
        //We don't create templatei for non-existing files or
        //files that cannot be read because no parser is specified.
        return nullptr;
    }

    //TODO: Open the source file and read its content.
    std::string content = "";

    //Create a Template instance:
    auto template_instance = std::make_shared<Template>(content, parser);
    //Read the template data:
    template_instance->read();
    return template_instance;
}


void Template::setAttribute(const std::string& name, std::shared_ptr<TemplateValue> value)
{
    this->attributes[name] = value;
}


std::string Template::render()
{
    std::string result = "";

    auto current_part = this->first_template_part;
    while (current_part != nullptr) {
        current_part->setAttributes(this->attributes);
        result += current_part->render();
        current_part = current_part->next();
    }

    return result;
}
