/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/*
 * This is a first test application to see how well
 * basic Jinja2 templates can be rendered in C++.
 */


#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>


#include "Template.h"
#include "TemplateData/TemplateValue.h"
#include "Parsers/Jinja2Parser.h"


const std::string jinja2_template_content = "Hello {{ subject }}! Read this random number: {{ number }} :-)";


int main()
{
    srand(time(NULL));
    int number = rand();

    auto jinja2_parser = std::make_shared<Cuwte::Jinja2Parser>();
    if (jinja2_parser == nullptr) {
        return 1;
    }
    auto t = std::make_shared<Cuwte::Template>(jinja2_template_content, jinja2_parser);
    if (t == nullptr) {
        return 1;
    }
    t->read();
    t->setAttribute("subject", std::make_shared<Cuwte::TemplateValue>("World"));
    t->setAttribute("number", std::make_shared<Cuwte::TemplateValue>(number));
    std::cout << t->render() << std::endl;

    return 0;
}
