/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_TEMPLATEPART
#define CUWTE_TEMPLATEPART


#include <map>
#include <memory>
#include <vector>


#include "TemplateTag.h"
#include "TemplateData/TemplateValue.h"


namespace Cuwte
{
    class TemplatePart
    {
        protected:


        /**
         * The source code of the template part.
         */
        std::string source = "";


        /**
         * This map contains the attributes that are filled in the
         * template part's placeholders.
         */
        std::map<std::string, std::shared_ptr<TemplateValue>> attributes;


        /**
         * This map stores the expressions and their positional data
         * so that they can easily be replaced during rendering.
         */
        std::map<std::string, std::vector<TemplateTag>> expressions;


        /**
         * The successor of this template in case the condition for
         * deciding the next template is true.
         * This pointer also holds the successor in case the is no condition,
         * meaning that the next template shall be rendered in any case.
         */
        std::shared_ptr<TemplatePart> condition_true_successor = nullptr;


        /**
         * The successor of this template in case the condition for
         * deciding the next template is false.
         */
        std::shared_ptr<TemplatePart> condition_false_successor = nullptr;


        public:


        /**
         * This method "loads" the source code of the template and
         * starts the template parsing process.
         *
         * @param const std::string& source The source code of the template
         *     that shall be parsed.
         */
        virtual void setSource(const std::string& source);


        /**
         * Sets the map with expression positions (the expressions attribute).
         */
        virtual void setExpressions(const std::map<std::string, std::vector<TemplateTag>> expressions);


        /**
         * Sets the attributes that are required for this template by looking
         * at a big list of attributes. Only those attributes that are needed
         * in the template are picked and copied from the attribute list.
         *
         * @param const std::map<std::string, std::shared_ptr<TemplateValue>>& attributes
         *     The attributes to be set.
         */
        virtual void setAttributes(const std::map<std::string, std::shared_ptr<TemplateValue>>& attributes);


        /**
         * Sets a value for a template attribute (placeholder) whose name shall
         * be replaced with the value during rendering.
         *
         * @param const std::string& name The name of the template attribute.
         *
         * @param const std::shared_ptr<TemplateValue> value The value of the template attribute.
         */
        virtual void setAttribute(const std::string& name, const std::shared_ptr<TemplateValue> value);


        /**
         * Renders the template.
         *
         * @returns std::string The rendered template.
         */
        virtual std::string render();


        /**
         * Determines which template part from the template part graph is the
         * next one and then returns a pointer to that template part. If no
         * successor exists, a nullptr is returned.
         *
         * @returns std::shared_ptr<TemplatePart> Either a TemplatePart instance
         *     that represents the successor of this template or a nullptr.
         */
        virtual std::shared_ptr<TemplatePart> next();
    };
}


#endif
