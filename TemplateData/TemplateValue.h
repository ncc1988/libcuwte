/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_TEMPLATEVALUE
#define CUWTE_TEMPLATEVALUE


#include <string>


namespace Cuwte
{
    /**
     * The TemplateValue class is an abstraction to make it more convenient
     * to pass different types of variables to templates.
     */
    class TemplateValue
    {
        protected:


        std::string display_value;


        public:


        TemplateValue();


        TemplateValue(const TemplateValue& other);


        //template<typename T> TemplateValue(const T& value);

        //only temporary:
        template<typename T> TemplateValue(const T& value)
        {
            if constexpr(std::is_integral<T>::value) {
                //Convert all integral types to a string.
                    this->display_value = std::to_string(value);
            } else {
                //TODO: more type handling
                this->display_value = value;
            }
        }


        const TemplateValue& operator=(const TemplateValue& other);


        template<typename T> const TemplateValue& operator=(const T& value);


        operator bool() const;


        std::string getDisplayValue() const;


        /**
         * NOTE: This method may become more useful in case the conversion
         * to a string vlue shall be done during template rendering
         * instead of template value reading.
         */
        std::string toString();
    };
}


#endif
