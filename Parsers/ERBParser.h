/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_ERBPARSER
#define CUWTE_ERBPARSER


#include "../Parser.h"


namespace Cuwte
{
    class ERBParser: public Parser
    {
        protected:


        /**
         * The ERB tags all begin with the same tow characters.
         * Only the third character makes a difference.
         */
        enum class TagType
        {
            NONE,       ///Not inside a tag at the moment.
            CODE,       ///The third character is neither "=" nor "#".
            EXPRESSION, ///The third character is "=".
            COMMENT     ///The third character is "#".
        };


        /**
         * The ERB parser states.
         */
        enum class State
        {
            TEXT,       ///Simple (unprocessed) text.
            TAG_START,  ///The start of a template tag.
            TAG_CONTENT ///The content of a tag.
        };


        /**
         * The last read characters when parsing a source.
         */
        char last_chars[2] = {0, 0};


        /**
         * The current state of the parser.
         */
        State state = State::TEXT;


        TagType current_tag_type = TagType::NONE;


        /**
         * The source block that is currently read and will become
         * the source of the next template part.
         */
        std::string current_part_source = "";


        public:


        ERBParser();


        virtual void reset() override;


        virtual std::shared_ptr<TemplatePart> parse(const std::string& source) override;
    };
}


#endif
