/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "ERBParser.h"


using namespace Cuwte;


ERBParser::ERBParser()
{
    //Nothing at the moment.
}


void ERBParser::reset()
{
    //TODO
}


std::shared_ptr<TemplatePart> ERBParser::parse(const std::string& source)
{
    auto template_tag_start = source.end();
    size_t template_tag_length = 0;
    for (auto it = source.begin(); it != source.end(); it++) {
        char c = *it;

        if (this->state == State::TEXT) {
            if ((c == '%') && (this->last_chars[0] == '<')) {
                //The start of a tag.
                this->state = State::TAG_START;
                template_tag_start = std::prev(it);
            }
        } else if (this->state == State::TAG_START) {
            //Read the third character of the tag to determine its type.
            if (c == '=') {
                this->current_tag_type = TagType::EXPRESSION;
            } else if (c == '#') {
                this->current_tag_type = TagType::COMMENT;
            } else {
                this->current_tag_type = TagType::CODE;
            }
            this->state = State::TAG_CONTENT;
        } else if (this->state == State::TAG_CONTENT) {
            if ((c == '>') && (this->last_chars[0] == '%')) {
                this->state = State::TEXT;

                //TODO: Depending on the type of the tag is must either
                //be removed during rendering (comment or code) or its
                //variables must be printed (expression).
                //In any case, its start and end positions must be collected
                //in the placeholder list.
            }
        }

        //Move the last character array items:
        this->last_chars[1] = this->last_chars[0];
        this->last_chars[0] = c;

        //Add the current char to the current template part source:
        this->current_part_source += c;
    }

    return nullptr; //To be implemented.
}
