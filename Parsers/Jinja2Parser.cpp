/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "Jinja2Parser.h"


using namespace Cuwte;


Jinja2Parser::Jinja2Parser()
{
    //Nothing at the moment.
}


void Jinja2Parser::reset()
{
    this->state = State::TEXT;
    this->last_chars[0] = 0;
    this->last_chars[1] = 0;
    this->last_chars[2] = 0;
    this->current_content = "";
    this->current_part_source = "";
}


std::shared_ptr<TemplatePart> Jinja2Parser::parse(const std::string& source)
{
    //Parse the template and create a "value map" that marks the
    //positions where to replace placeholders with values.

    auto template_tag_start = source.end();
    size_t template_tag_length = 0;
    for (auto it = source.begin(); it != source.end(); it++) {
        char c = *it;
        if (this->state == State::TEXT) {
            if ((c == '{') && last_chars[0] == '{') {
                this->state = State::EXPRESSION_START;
                template_tag_start = std::prev(it);
            } else if ((c == '%') && last_chars[0] == '{') {
                this->state == State::STATEMENT_START;
                template_tag_start = std::prev(it);
                //TODO: remove the last char from current_part_source
                //and stop adding data to current_part_source.
                //The statement will create a new template part when it is
                //completely read.
            } else if ((c == '#') && last_chars[0] == '{') {
                this->state == State::COMMENT_START;
                template_tag_start = std::prev(it);
            }
        } else if (this->state == State::EXPRESSION_START) {
            if ((c != ' ')) {
                //The first character of the content has been read.
                this->state = State::EXPRESSION_CONTENT;
                this->current_content += c;
            }
        } else if (this->state == State::EXPRESSION_CONTENT) {
            //At the moment, we do not support whitespace in the
            //expression content.
            if (c == ' ') {
                //The content has finished.
                this->state = State::EXPRESSION_END;
            } else {
                this->current_content += c;
            }
        } else if (this->state == State::EXPRESSION_END) {
            if ((c == '}') && (this->last_chars[0] == '}')) {
                //The expression has ended. Add it to the
                //expression list.
                template_tag_length = std::distance(template_tag_start, it) + 1;

                //If there is already an entry for the expression,
                //we add this new occurrence to it.
                auto occurrence = this->current_expressions.find(this->current_content);
                if (occurrence == this->current_expressions.end()) {
                    //Add an entry for the expression.
                    this->current_expressions.insert(
                        std::pair<std::string, std::vector<TemplateTag>>(
                            this->current_content,
                            {
                                TemplateTag(
                                    std::distance(source.begin(), template_tag_start),
                                    template_tag_length
                                    )
                            }
                            )
                        );
                    this->state = State::TEXT;
                } else {
                    //Add another occurrence.
                    occurrence->second.push_back(
                        TemplateTag(
                            std::distance(source.begin(), template_tag_start),
                            template_tag_length
                            )
                        );
                }
                this->current_content = "";
                template_tag_start = source.end();
                template_tag_length = 0;
            }
        } else if (this->state == State::STATEMENT_START) {
            //Begin to read the statement and identify variables,
            //keywords and constants.
            if ((c == 'f') && (last_chars[0] == 'i')) {
                //if-statement begin read.
                //Read the condition.
                this->current_statement_type = StatementType::IF;
                this->state = State::STATEMENT_CONDITION;
            }
        }

        //Move the last character array items:
        this->last_chars[2] = this->last_chars[1];
        this->last_chars[1] = this->last_chars[0];
        this->last_chars[0] = c;

        //Add the current char to the current template part source:
        this->current_part_source += c;
    }

    //At this point, the source string has been read completely.
    //The parser cannot know if this is also the end of the whole
    //source or ontly the end of a chunk. It assumes that the whole
    //source has been parsed and at least one template part is available
    //that can be returned.

    if (this->first_part == nullptr) {
        //Initialise the first part pointer and set the last part pointer
        //to it, since there is only one template part.
        this->first_part = std::make_shared<TemplatePart>();
        this->last_part = this->first_part;
    }

    if (this->last_part == nullptr) {
        //Something went wrong.
        return nullptr;
    }

    this->last_part->setSource(this->current_part_source);
    this->last_part->setExpressions(this->current_expressions);
    return this->first_part;
}
