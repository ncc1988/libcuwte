/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_JINJA2PARSER
#define CUWTE_JINJA2PARSER


#include <iterator>
#include <map>
#include <vector>


#include "../Parser.h"
#include "../TemplatePart.h"
#include "../TemplateData/TemplateValue.h"
#include "../TemplateTag.h"


namespace Cuwte
{
    class Jinja2Parser: public Parser
    {
        protected:


        enum class State
        {
            TEXT, //Simple (unprocessed) text.
            EXPRESSION_START, //The begin of a expression has been read,
                              //but its content hasn't started yet.
            EXPRESSION_CONTENT, //The content of the placeholder started.
            EXPRESSION_END,  //The content of the expression has been read,
                             //but the closing curly braces haven't been read yet.
            //The following states are analog to the EXPRESSION states.
            STATEMENT_START,
            STATEMENT_CONTENT,
            STATEMENT_END,
            //The content of comments don't matter here.
            COMMENT_START,
            COMMENT_END
        };


        /**
         * The last read characters.
         */
        char last_chars[3] = {0,0,0};


        /**
         * The current parser state.
         */
        State state = State::TEXT;


        /**
         * The current parsing content.
         */
        std::string current_content = "";


        /**
         * The source of the current template part.
         * This is not the current parsing content since the latter
         * only regards text relevant after or during parsing.
         */
        std::string current_part_source = "";


        /**
         * This map stores the expressions of the current template part and
         * the expression's positional data so that they can easily be replaced
         * during rendering.
         */
        std::map<std::string, std::vector<TemplateTag>> current_expressions;


        /**
         * The first parsed template part.
         */
        std::shared_ptr<TemplatePart> first_part = nullptr;


        /**
         * The last parsed template part.
         * This is the latest template part that has been parsed and not
         * a pointer to the last part of the template unless the template
         * has been fully parsed.
         */
        std::shared_ptr<TemplatePart> last_part = nullptr;


        public:


        Jinja2Parser();


        virtual void reset() override;


        virtual std::shared_ptr<TemplatePart> parse(const std::string& source) override;
    };
}


#endif
