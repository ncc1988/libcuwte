/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "TemplatePart.h"


using namespace Cuwte;


void TemplatePart::setSource(const std::string& source)
{
    this->source = source;
}


void TemplatePart::setExpressions(const std::map<std::string, std::vector<TemplateTag>> expressions)
{
    this->expressions = expressions;
}


void TemplatePart::setAttributes(const std::map<std::string, std::shared_ptr<TemplateValue>>& attributes)
{
    this->attributes = attributes;
}


void TemplatePart::setAttribute(const std::string& name, const std::shared_ptr<TemplateValue> value)
{
    if (name.empty() || !value) {
        return;
    }
    this->attributes[name] = value;
}


std::string TemplatePart::render()
{
    std::string output = this->source;
    for (auto expression: this->expressions) {
        //Replace the expression tag with the value of the attribute
        //with the same name or replace it with an empty string if no
        //such attribute exists.
        std::string replacement = "";
        auto attribute = this->attributes.find(expression.first);
        if (attribute != this->attributes.end()) {
            //There is an attribute with that name.
            if (attribute->second != nullptr) {
                replacement = attribute->second->toString();
            }
        }

        //Now we can replace all tags for this expression:
        for (auto occurrence: expression.second) {
            output.replace(occurrence.position, occurrence.length, replacement);
        }
    }

    return output;
}


std::shared_ptr<TemplatePart> TemplatePart::next()
{
    //TODO:
    //In case there is a jump condition, calculate the condition.
    //Based on the result, return either the condition_true or the
    //condition_false template.
    return this->condition_true_successor; //to be implemented
}
