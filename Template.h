/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_TEMPLATE
#define CUWTE_TEMPLATE


#include <map>
#include <memory>

#include "Parser.h"
#include "TemplatePart.h"
#include "TemplateData/TemplateValue.h"


namespace Cuwte
{
    class Template
    {
        protected:


        /**
         * The source code of the template.
         */
        std::string source = "";

        /**
         * The template parser that shall be used to parse the
         * template file.
         */
        std::shared_ptr<Parser> parser = nullptr;


        /**
         * A pointer to the first template part in the graph of template parts
         * that have been created after parsing the source of this template.
         */
        std::shared_ptr<TemplatePart> first_template_part = nullptr;


        /**
         * This map contains the template attributes that are filled in the
         * template's placeholders.
         */
        std::map<std::string, std::shared_ptr<TemplateValue>> attributes;


        public:


        /**
         * Constructs a Template instance and directly specifies
         * the template source and the parser to be used to parse
         * the template.
         */
        Template(
            const std::string& source = "",
            std::shared_ptr<Parser> parser = nullptr
            );


        /**
         * This method reads the template file and passes the data to the
         * parser which will construct Template objects from the template
         * source code.
         */
        virtual void read();


        /**
         * This helper method simplifies the process of reading a template
         * from a local file.
         *
         * @param const std::string& file_name The file name to read the
         *     template from.
         *
         * @param std::shared_ptr<TemplateParser> The parser to be used
         *     to read the template file.
         *
         * @returns std::shared_ptr<TemplateFile> On success, a Template
         *     is returned. In case of an error, a nullptr is returned.
         */
        static std::shared_ptr<Template> readFromFile(
            const std::string& file_name,
            std::shared_ptr<Parser> parser
            );


        /**
         * Sets a value for a template attribute (placeholder) whose name shall
         * be replaced with the value during rendering.
         *
         * @param const std::string& name The name of the template attribute.
         *
         * @param const Cuwte::TemplateValue& value The value of the
         *     template attribute.
         */
        virtual void setAttribute(const std::string& name, std::shared_ptr<TemplateValue> value);


        /**
         * Renders the template by traversing the TemplatePart.
         *
         * @returns std::string The rendered template.
         */
        virtual std::string render();
    };
}


#endif
