/*
 *  This file is part of libcuwte
 *  Copyright (C) 2020  Moritz Strohm
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CUWTE_PARSER
#define CUWTE_PARSER


#include "TemplatePart.h"


namespace Cuwte
{
    class Parser
    {

        public:


        /**
         * Resets the parser to its initial state.
         */
        virtual void reset() = 0;


        /**
         * Parses the template source and returns the template part chain
         * for the template by returning the first chain item. If the source
         * is empty, a nullptr is returned.
         *
         * @param const std::string& source The template source as string.
         *
         * @returns std::shared_ptr<TemplatePart> The first template part.
         */
        virtual std::shared_ptr<TemplatePart> parse(const std::string& source = "") = 0;
    };
}


#endif
